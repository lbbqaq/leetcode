package com.leetcode;

import java.util.Arrays;

/**
 * 给你一个字符串 time ，格式为 hh:mm（小时：分钟），其中某几位数字被隐藏（用 ? 表示）。
 *
 * 有效的时间为 00:00 到 23:59 之间的所有时间，包括 00:00 和 23:59 。
 *
 * 替换 time 中隐藏的数字，返回你可以得到的最晚有效时间。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/latest-time-by-replacing-hidden-digits
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class atest_time_by_replacing_hidden_digits_1736 {

    public static void main(String[] args) {

        String time= "2?:?0";

        System.out.println(solution(time));


    }

    public static String solution(String time ){

        //最后返回的时候可以反回成原来的形式
       char[] s= time.toCharArray();

       //s[0]只有1或2，如果s[1]大于4并且小于9那么就为1，否则为2
       if(s[0]=='?'){
           s[0]=('4'<=s[1]&&s[1]<='9')?'1':'2';
       }
       //如果s[0]为2，那么s[1]就为3，否则为9
       if(s[1]=='?'){
           s[1]=(s[0]=='2')?'3':'9';
       }
       //s[3]最大为5
       if(s[3]=='?'){
           s[3]='5';
       }
       //s[4]最大为9
       if(s[4]=='?'){
           s[4]='9';
       }
       return new String(s);
    }
}
