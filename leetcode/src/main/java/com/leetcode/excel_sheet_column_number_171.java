package com.leetcode;


/**
 * 给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回该列名称对应的列序号。
 *   A -> 1
 *     B -> 2
 *     C -> 3
 *     ...
 *     Z -> 26
 *     AA -> 27
 *     AB -> 28
 *     ...
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/excel-sheet-column-number
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class excel_sheet_column_number_171 {




    public int titleToNumber(String columnTitle) {

        int sum =0;
        for (int i = 0; i < columnTitle.length(); i++) {
            int num=columnTitle.charAt(i)-'A'+1;
            sum=sum*26+num;
        }

        return sum;

    }
}
