package com.leetcode;

import java.util.Arrays;

/**
 * 给你一个二维整数数组 ranges 和两个整数 left 和 right 。每个 ranges[i] = [starti, endi] 表示一个从 starti 到 endi 的 闭区间 。
 *
 * 如果闭区间 [left, right] 内每个整数都被 ranges 中 至少一个 区间覆盖，那么请你返回 true ，否则返回 false 。
 *
 * 已知区间 ranges[i] = [starti, endi] ，如果整数 x 满足 starti <= x <= endi ，那么我们称整数x 被覆盖了。
 *
 *  
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode_cn.com/problems/check_if_all_the_integers_in_a_range_are_covered
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class check_if_all_the_integers_in_a_range_are_covered_1893 {

    public static void main(String[] args) {

    }

    public static boolean solution(int[][] ranges, int left, int right){


        return false;
    }
}
